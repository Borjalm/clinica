<!DOCTYPE html>
<html lang="es">

<head>
    <title>rol</title>
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>

<body>
    <div id="inicio">
        <h1><span id="centros">CENTROS</span></br>
            <span id="cuida">CUIDA- T!</span> <img src="css/logo.png" />
        </h1>
    </div>
    <?php
    session_start();
    if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
    }

    ?>
    <div id="contenedor">
    <div class="usuario">
        <p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
                <b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
    </div>
    <form action="" method="POST">
        <input type="submit" name="citasAt" value="CITAS ATENDIDAS" class="botonesrol" /><br /><br />
        <input type="submit" name="nueva" value="NUEVA CITA" class="botonesrol" /><br /><br />
        <input type="submit" name="altaPac" value="ALTA PACIENTE" class="botonesrol" /><br /><br />
        <input type="submit" name="verPac" value="PACIENTES" class="botonesrol" /><br /><br />
        <input type="submit" name="Cierre" value="CERRAR" class="botonesrol" />
    </form>
    <?php

    if (isset($_POST["citasAt"]) ||  isset($_POST["altaPac"]) || isset($_POST["nueva"]) || isset($_POST["Cierre"]) || isset($_POST["verPac"])) {

        if ($_POST["altaPac"]) {
            header("Location:rolasis/altapaciente.php");
        } elseif ($_POST["nueva"]) {
            header("Location:rolasis/nuevacita.php");
        } elseif ($_POST["citasAt"]) {
            header("Location:rolasis/vercitas.php");
        } elseif ($_POST["verPac"]) {
            header("Location:rolasis/pacientes.php");
        } else {
            session_destroy();
            header("Location:../index.php");
        }
    }
    ?>
</div>

</body>

</html>