<!DOCTYPE html>
<html lang="es">

<head>
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
    <div id="inicio">
        <h1><span id="centros">CENTROS</span></br>
            <span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
        </h1>
    </div>
    <?php
    session_start();
    if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
    }
    ?>
    <div id="contenedor">
        <div class="usuario">
            <p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
                    <b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
        </div>
        <table>
            <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Paciente</th>
                <th>Médico</th>
                <th>Consultorio</th>
                <th>Estado</th>
                <th>Observaciones</th>
            </tr>
            <?php $conexion = mysqli_connect("localhost", "Medico", "1234", "consulta");

            $sql = "SELECT 
            citas.citFecha,citas.citHora,
            pacientes.dniPac,pacientes.pacNombres,
            citas.citPaciente,medicos.dniMed,citas.citMedico,medicos.medNombres,
            citas.citConsultorio,consultorios.idConsultorio,consultorios.conNombre,
            citas.citEstado,citas.CitObservaciones
            FROM pacientes 
            INNER JOIN citas ON pacientes.dniPac=citas.citPaciente 
            INNER JOIN medicos ON medicos.dniMed=citas.citMedico 
            INNER JOIN consultorios ON consultorios.idConsultorio=citas.citConsultorio";
            $resultado = mysqli_query($conexion, $sql);
            if (mysqli_num_rows($resultado) > 0) {
                while ($registro = mysqli_fetch_row($resultado)) {
                    $fecha = $registro[0];
                    $hora = $registro[1];
                    $paciente = $registro[3];
                    $dnimed = $registro[6];
                    $medico = $registro[7];
                    $consultorio = $registro[10];
                    $estado = $registro[11];
                    $observaciones = $registro[12];
                    if ($observaciones != " " && $dnimed == $_SESSION["dniusuario"]) {
                        echo "<tr><td>" . date("d/m/Y", strtotime($fecha)) . "</td>";
                        echo "<td>" . date("H:i", strtotime($hora)) . "</td>";
                        echo "<td>" . $paciente . "</td>";
                        echo "<td>" . $medico . "</td>";
                        echo "<td>" . $consultorio . "</td>";
                        echo "<td>" . $estado . "</td>";
                        echo "<td>" . $observaciones . "</td>";
                    }
                }
            } else {
                echo "<table><td>No tienes citas atendidas</td></table";
            }
            ?>
        </table>
        <form action="" method="POST">
            <input type="submit" name="atras" value="ATRÁS" class="boton" />
            <input type="submit" name="cierre" value="CERRAR" class="boton" />
            <?php
            if (isset($_POST["atras"])) {
                header("Location: ../medico.php");
            } elseif (isset($_POST["Cierre"])) {
                mysqli_close($conexion);
                session_destroy();
                header("Location: ../../index.php");
            }
            ?>
        </form>
    </div>
</body>

</html>