<!DOCTYPE html>
<html lang="es">

<head>
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
	<div id="inicio">
		<h1><span id="centros">CENTROS</span></br>
			<span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
		</h1>
	</div>
	<?php
	session_start();

	if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
	}
	?>
	<div id="contenedor">
		<div class="usuario">
			<p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
					<b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
		</div>
		<script>
			function validacion() {
				var email = document.getElementById("ema").value;
				var nombre = document.getElementById("nom").value;
				var apell = document.getElementById("ape").value;
				var contra = document.getElementById("con").value;
				var confirmarcontra = document.getElementById("concon").value;
				var dni = document.getElementById("dn").value;
				var estado = document.getElementById("eleccion").value;
				var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
				var expresion_regular_dni = /(^([0-9]{8,8}\-[A-Z])|^)$/;
				var expRegNombre = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
				var expRegApellidos = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;

				if (nombre.length < 3 || !expRegNombre.test(nombre)) {
					alert("Nombre no válido");
					return false;
				}
				if (!expRegApellidos.test(apell)) {
					alert("Apellidos no válidos");
					return false;
				}
				if (!expresion.test(email)) {
					alert("La dirección de email " + email + " no es válida.");
					return false;
				}
				if (contra != confirmarcontra) {
					alert("Las contraseñas no coinciden");
					return false;
				}
				if (!expresion_regular_dni.test(dni)) {
					alert("dni no válido");
					return false;
				}
				return true;
			}
		</script>
		<div id="formulario">
			<form action="" method="POST" onsubmit="return validacion()">
				<p>Nombre</br><input type="text" name="nombre" id="nom" required /></p>
				<p>Apellidos</br><input type="text" name="apellidos" minlength="10" maxlength="40" id="ape" required /></p>
				<p>Especialidad</br><input type="text" name="especialidad" id="espe" required /><br />
					<p>Teléfono</br><input type="text" name="telefono" pattern="^[5-9][0-9]{8}" id="tel" required /></p>
					<p>Email</br><input type="text" name="email" id="ema" required /></p>
					<p>DNI</br><input type="text" name="dni" id="dn" required /></p>
					<p>Nombre de usuario</br> <input type="text" name="nomusu" id="nousu" maxlength="15" required /></p>
					<p>Contraseña</br> <input type="password" name="contra" id="con" required /></p>
					<p>Confirmar contraseña</br> <input type="password" name="confcontra" id="concon" required /></p>
					<p>Estado<select name="estado" id="eleccion"></p>
					<option value="Activo" selected>Activo</option>
					<option value="Inactivo">Inactivo</option>
					</select>

					<input type="submit" name="Enviar" value="ALTA MÉDICO" class="botonesalta" />
			</form>
			<form action="" method="POST">
				<input type="submit" name="atras" value="ATRÁS" class="boton" />
				<input type="submit" name="cierre" value="CERRAR" class="boton" />
			</form>
		</div>

		<?php

		if (isset($_POST["Enviar"])) {
			$nombre = $_POST["nombre"];
			$apellidos = $_POST["apellidos"];
			$especialidad = $_POST["especialidad"];
			$telefono = $_POST["telefono"];
			$email = $_POST["email"];
			$dni = $_POST["dni"];
			$nombreusu = $_POST["nomusu"];
			$contra = $_POST["confcontra"];
			$estado = $_POST["estado"];
			$tipo = "Medico";
			$conexion = mysqli_connect("localhost", "admin", "1234", "consulta");
			$sql = "INSERT INTO usuarios (dniUsu, usuLogin,usuPassword,usuEstado,usutipo)
		VALUES ('$dni','$nombreusu','$contra','$estado','$tipo')";
			$sql2 = "INSERT INTO medicos (dniMed, medNombres,medApellidos,medEspecialidad,medTelefono,medCorreo)
			VALUES ('$dni','$nombre','$apellidos','$especialidad','$telefono','$email')";
			if (mysqli_query($conexion, $sql) && mysqli_query($conexion, $sql2)) {
				echo "<br> Médico registrado";
			}
		}
		?>

		<?php
		if (isset($_POST["cierre"])) {
			session_destroy();
			mysqli_close($conexion);
			header("Location: ../../index.php");
		}
		if (isset($_POST["atras"])) {

			header("Location: ../medico.php");
		}
		?>
	</div>


</body>

</html>