<!DOCTYPE html>
<html lang="es">

<head>
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
	<div id="inicio">
		<h1><span id="centros">CENTROS</span></br>
			<span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
		</h1>
	</div>
	<?php
	session_start();
	if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
		function menor10($dato)
		{
			if ($dato < 10) {
				$dato = "0" . $dato;
			}
			return $dato;
		}
		$hoy = getdate();

		$fechainicio = $hoy['year'] - 100 . "-01-01";
		$fechafin = $hoy['year'] . "-" . menor10($hoy['mon']) . "-" . menor10($hoy['mday']);
	}
	?>
	<div id="contenedor">
		<div class="usuario">
			<p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
					<b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
		</div>

		<script>
			function validacion() {
				var nombre = document.getElementById("nom").value;
				var apell = document.getElementById("ape").value;
				var contra = document.getElementById("con").value;
				var confirmarcontra = document.getElementById("concon").value;
				var dni = document.getElementById("dn").value;
				var estado = document.getElementById("eleccion").value;
				var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
				var expresion_regular_dni = /(^([0-9]{8,8}\-[A-Z])|^)$/;
				var expRegNombre = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
				var expRegApellidos = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;

				if (nombre.length < 3 || !expRegNombre.test(nombre)) {
					alert("Nombre no válido");
					return false;
				}
				if (!expRegApellidos.test(apell)) {
					alert("Apellidos no válidos");
					return false;
				}
				if (contra != confirmarcontra) {
					alert("Las contraseñas no coinciden");
					return false;
				}
				if (!expresion_regular_dni.test(dni)) {
					alert("dni no válido");
					return false;
				}
				return true;
			}
		</script>
		<div id="formulario">
			<form action="" method="POST" onsubmit="return validacion()">
				<p>Nombre</br> <input type="text" id="nom" name="nombre" required /></p>
				<p>Apellidos</br> <input type="text" id="ape" name="apellidos" required /></p>
				<p>DNI</br> <input type="text" id="dn" name="dni" required /><br />
					<p>Fecha de nacimiento</br><input type="date" name="fecha" min=<?php echo $fechainicio;  ?> max=<?php echo $fechafin; ?> required /></p>
					<p>Nombre de usuario</br> <input type="text" name="nomusu" required /></p>
					<p>Contraseña</br> <input type="password" id="con" required /></p>
					<p>Repetir contraseña</br> <input type="password" id="concon" name="confcontra" required /> </p>
					<p>Sexo<select name="sexo" id="eleccion" required></p>
					<option value="femenino" selected>Femenino</option>
					<option value="masculino">Masculino</option>
					</select><br />

					<p>Estado<select name="estado" id="est" required></p>
					<option value="Activo" selected>Activo</option>
					<option value="Inactivo">Inactivo</option><br />
					</select>
					<input type="submit" name="Enviar" value="ALTA PACIENTE" class="botonesalta" />
			</form>
			<form action="" method="POST">
				<input type="submit" name="atras" value="ATRÁS" class="boton" />
				<input type="submit" name="cierre" value="CERRAR" class="boton" />
			</form>
		</div>
		<?php


		if (isset($_POST["Enviar"])) {


			$nombre = $_POST["nombre"];
			$apellidos = $_POST["apellidos"];
			$dni = $_POST["dni"];
			$fecha = $_POST["fecha"];
			$sexo = $_POST["sexo"];
			$estado = $_POST["estado"];
			$nombreusu = $_POST["nomusu"];
			$contra = $_POST["confcontra"];
			$tipo = "Paciente";
			$conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
			$sql = "INSERT INTO usuarios (dniUsu, usuLogin,usuPassword,usuEstado,usutipo)
		VALUES ('$dni','$nombreusu','$contra','$estado','$tipo')";
			$sql2 = "INSERT INTO pacientes (dniPac, pacNombres,pacApellidos,pacFechaNacimiento,pacSexo)
			VALUES ('$dni','$nombre','$apellidos','$fecha','$sexo')";
			if (mysqli_query($conexion, $sql) && mysqli_query($conexion, $sql2)) {
				echo "<br> Paciente registrado";
			}
		}
		if (isset($_POST["cierre"])) {
			session_destroy();
			mysqli_close($conexion);
			header("Location: ../../index.php");
		}
		if (isset($_POST["atras"])) {
			header("Location: ../asistente.php");
		}
		?>
	</div>
</body>

</html>