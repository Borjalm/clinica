<!DOCTYPE html>
<html lang="es">

<head>
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
	<div id="inicio">
		<h1><span id="centros">CENTROS</span></br>
			<span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
		</h1>
	</div>
	<?php
	session_start();
	if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
	}

	?>
	<div id="contenedor">
		<div class="usuario">
			<p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
					<b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
		</div>

		<table id="cincocampos">
			<tr>
				<th>Identificación</th>
				<th>Nombre</th>
				<th>Apellidos</th>
				<th>Fecha de nacimiento</th>
				<th>Sexo</th>
			</tr>
			<?php $conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
			$sql = "SELECT dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo FROM pacientes";
			$resultado = mysqli_query($conexion, $sql);
			if (mysqli_num_rows($resultado) > 0) {
				while ($registro = mysqli_fetch_row($resultado)) {
					$dni = $registro[0];
					$nombre = $registro[1];
					$apell = $registro[2];
					$fecha = $registro[3];
					$sexo = $registro[4];
					echo "<tr><td>" . $dni . "</td>";
					echo "<td>" . $nombre . "</td>";
					echo "<td>" . $apell . "</td>";
					echo "<td>" . date("d/m/Y", strtotime($fecha)) . "</td>";
					echo "<td>" . $sexo . "</td></tr>";
				}
			}
			?>
		</table>
		<form action="" method="POST">

			<input type="submit" name="atras" value="ATRÁS" class="boton" />
			<input type="submit" name="cierre" value="CERRAR" class="boton" />
		</form>
		<?php
		if (isset($_POST["cierre"])) {

			session_destroy();
			mysqli_close($conexion);
			header("Location: ../../index.php");
		}
		if (isset($_POST["atras"])) {
			header("Location: ../asistente.php");
		}
		?>
	</div>
</body>

</html>