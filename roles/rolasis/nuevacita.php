<!DOCTYPE html>
<html lang="es">

<head>
	<link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
	<div id="inicio">
		<h1><span id="centros">CENTROS</span></br>
			<span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
		</h1>
	</div>

	<?php
	session_start();
	if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
	}
	?>
	<div id="contenedor">
		<div class="usuario">
			<p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
					<b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
		</div>
		<div id="formulario">
			<form action="" method="POST">
				Paciente<select name="paciente" required><br />
					<option name="seleccione">Seleccione <br />
					</option>
					<?php $conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
					$sql = "SELECT pacNombres,dniPac FROM pacientes";
					$resultado = mysqli_query($conexion, $sql);
					if (mysqli_num_rows($resultado) > 0) {
						while ($registro = mysqli_fetch_row($resultado)) {
							$nombrepac = $registro[0];
							$dnipac = $registro[1];
							echo "<option value='$dnipac' required>" . $nombrepac . "</option>";
						}
					}
					?>
				</select><br />
				<p>Fecha</br><input type="date" name="fecha" min="2020-01-01" max="2021-06-22" required /></p>
				<p>Hora</br> <input type="time" name="hora" min="09:00" max="15:00" required /></p>
				<p>Médico<select name="medico" required></p>
				<option name="seleccione">Seleccione
				</option>
				<?php $conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
				$sql = "SELECT medNombres,dniMed FROM medicos";
				$resultado = mysqli_query($conexion, $sql);
				if (mysqli_num_rows($resultado) > 0) {
					while ($registro = mysqli_fetch_row($resultado)) {
						$nombremed = $registro[0];
						$dnimed = $registro[1];
						echo "<option value='$dnimed' required>" . $nombremed . "</option>";
					}
				}
				?>
				</select><br />
				<p>Consultorio<select name="consultorio" required></p>
				<option name="seleccione">Seleccione <br />
				</option>
				<?php $conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
				$sql = "SELECT conNombre,idConsultorio FROM consultorios";
				$resultado = mysqli_query($conexion, $sql);
				if (mysqli_num_rows($resultado) > 0) {
					while ($registro = mysqli_fetch_row($resultado)) {
						$nombrecon = $registro[0];
						$idconsul = $registro[1];
						echo "<option value='$idconsul' required>" . $nombrecon . "</option>";
					}
				}
				?>
				</select>
				</br>

				<input type="submit" name="Enviar" value="NUEVA CITA" class="botonesalta" />

			</form>
			<form action="" method="POST">
				<input type="submit" name="atras" value="ATRÁS" class="boton" />
				<input type="submit" name="cierre" value="CERRAR" class="boton" />
			</form>
		</div>
		<?php

		if (isset($_POST["Enviar"])) {

			$fecha = $_POST["fecha"];
			$hora = $_POST["hora"];
			$paciente = $_POST["paciente"];
			$medico = $_POST["medico"];
			$consultorio = $_POST["consultorio"];
			$observaciones = " ";
			$estado = "Asignado";
			$conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");
			$sql = "INSERT INTO citas (idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado,CitObservaciones)
		VALUES ('','$fecha','$hora','$paciente','$medico','$consultorio','$estado','$observaciones')";
			if (mysqli_query($conexion, $sql)) {
				echo "<br> Cita creada";
			} else {
				echo $conexion . "<br/>" . $sql;
			}
		}
		if (isset($_POST["cierre"])) {
			session_destroy();
			mysqli_close($conexion);
			header("Location: ../../index.php");
		}
		if (isset($_POST["atras"])) {
			header("Location: ../asistente.php");
		}

		?>
	</div>
</body>

</html>