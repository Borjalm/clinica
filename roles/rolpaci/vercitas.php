<!DOCTYPE html>
<html lang="es">

<head>
    <link rel="stylesheet" type="text/css" href="../css/estilo.css">
</head>

<body>
    <div id="inicio">
        <h1><span id="centros">CENTROS</span></br>
            <span id="cuida">CUIDA- T!</span> <img src="../css/logo.png" />
        </h1>
    </div>

    <?php
    session_start();
    if (isset($_SESSION["usuario"]) && isset($_SESSION["roles"])) {
    }

    ?>
    <div id="contenedor">
        <div class="usuario">
            <p><b>Nombre:<b> <?php echo $_SESSION["usuario"] ?></b><br />
                    <b>Conectado:<?php echo $_SESSION["roles"] ?></b></p>
        </div>
        <table>
            <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Usuario</th>
                <th>Médico</th>
                <th>Consultorio</th>
            </tr>
            <?php
            $conexion = mysqli_connect("localhost", "Asistente", "1234", "consulta");

            $sql = "SELECT 
            citas.citFecha,citas.citHora,
            pacientes.dniPac,pacientes.pacNombres,
            citas.citPaciente,medicos.dniMed,citas.citMedico,medicos.medNombres,
            citas.citConsultorio,consultorios.idConsultorio,consultorios.conNombre,
            citas.citEstado,citas.CitObservaciones 
            FROM pacientes 
            INNER JOIN citas ON pacientes.dniPac=citas.citPaciente 
            INNER JOIN medicos ON medicos.dniMed=citas.citMedico 
            INNER JOIN consultorios ON consultorios.idConsultorio=citas.citConsultorio";
            $resultado = mysqli_query($conexion, $sql);
            if (mysqli_num_rows($resultado) > 0) {
                while ($registro = mysqli_fetch_row($resultado)) {

                    $fecha = $registro[0];
                    $hora = $registro[1];
                    $paciente = $registro[3];
                    $dnipaci = $registro[4];
                    $medico = $registro[7];
                    $consultorio = $registro[10];
                    $estado = $registro[11];
                    $observaciones = $registro[12];
                    if ($dnipaci == $_SESSION["dniusuario"] && $observaciones==" ") {
                        echo "<tr><td>" . date("d/m/Y", strtotime($fecha)). "</td>";
                        echo "<td>" . date("H:i", strtotime($hora)). "</td>";
                        echo "<td>" . $paciente . "</td>";
                        echo "<td>" . $medico . "</td>";
                        echo "<td>" . $consultorio . "</td>";
                       
                  
                    } 
                }
            }
            ?>
        </table>
        <form action=" " method="POST">
            <input type="submit" name="Cierre" value="CERRAR" class="boton" />
        </form>
        <?php
        if (isset($_POST["Cierre"])) {
            session_destroy();
            mysqli_close($conexion);
            header("Location: ../../index.php");
        }
        ?>
    </div>
</body>

</html>